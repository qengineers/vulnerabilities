

from django.conf.urls import patterns, include, url
from django.contrib import admin

from blog import urls as blog_urls

admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'^admin/', include(admin.site.urls)),

    url(r'', include(blog_urls)),
)
