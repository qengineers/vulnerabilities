

from django.core.urlresolvers import reverse_lazy
from django.db import connection
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views.generic import ListView, View

from .models import Post


class PostListView(ListView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)

        sql = 'SELECT title, content, created FROM blog_post '
        if 'filter' in self.request.GET:
            sql += 'WHERE title LIKE \'' + self.request.GET['filter'] + '%\' '

        sql += 'ORDER BY created DESC'

        cursor = connection.cursor()
        cursor.execute(sql)

        context['posts'] = [
            dict(zip([col[0] for col in cursor.description], row))
            for row in cursor.fetchall()
        ]

        return context


class PostCreateView(View):
    model = Post

    success_url = reverse_lazy('post_list')

    def post(self, request, *args, **kwargs):
        sql = 'INSERT INTO blog_post (title, content, created) VALUES (\'{}\', \'{}\', \'{}\');'.format(
            request.POST['title'],
            request.POST['content'],
            timezone.now(),
        )

        cursor = connection.cursor()
        cursor.execute(sql)

        return HttpResponseRedirect(self.success_url)
