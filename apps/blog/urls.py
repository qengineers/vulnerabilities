

from django.conf.urls import patterns, url
from django.views.decorators.csrf import csrf_exempt

from blog.views import PostListView, PostCreateView


urlpatterns = patterns(
    '',

    url(r'^$', PostListView.as_view(), name='post_list'),
    url(r'^new/$', csrf_exempt(PostCreateView.as_view()), name='post_new'),
)
